﻿using DataAccess.Iterfaces;
using Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Implementation
{
    public class Repository<TEntity, TKey> : IRepository<TEntity, TKey>
        where TEntity : BaseEntity<TKey>
    {
        private readonly AppDbContext _db;
        private readonly DbSet<TEntity> _dbSet;

        public Repository(AppDbContext db)
        {
            _db = db;
            _dbSet = _db.Set<TEntity>();
        }

        public async Task<TEntity> AddAsync(TEntity item)
        {
            var result = await _dbSet.AddAsync(item);
            return result.Entity;
        }

        public void DeleteById(TKey id)
        {
            if(id== null) throw new ArgumentNullException("id");
            var targetItem = _dbSet.FirstOrDefault(s => s.Id.ToString() == id.ToString());
            if (targetItem != null)
            {
                _dbSet.Remove(targetItem);
            }
        }

        public async Task<TEntity?> GetById(TKey id)
        {
            if (id == null) throw new ArgumentNullException("id");
            return await _dbSet.FirstOrDefaultAsync(s => s.Id.ToString() == id.ToString());
        }

        public TEntity Update(TEntity item)
        {
            var result = _dbSet.Update(item);
            return result.Entity;
        }

        public void SaveChanges()
        {
            _db.SaveChanges();
        }
    }
}
