﻿using DataAccess.Iterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Implementation
{
    public class FileRepository : Repository<Entities.File, long>,IFileRepository
    {
        public FileRepository(AppDbContext db) : base(db)
        {
        }
    }
}
