using Controllers;
using DataAccess.Implementation;
using DataAccess.Iterfaces;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using System.Reflection;
using UseCases;
using Utils;
using Microsoft.Extensions.Configuration;
using SocialNetwork.Common.IdentityServer;

List<Assembly> dummyAssemblyList = new()
{
                    typeof(Controllers.AssemblyMarker).Assembly,
};
var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddByRegistrators();

builder.Services.AddDbContext<AppDbContext>(o =>
    o.UseNpgsql(new NpgsqlConnection(builder.Configuration.GetConnectionString("DefaultConnection")))
    .UseSnakeCaseNamingConvention());
builder.Services.AddTransient<IFileRepository, FileRepository>();
builder.Services.AddTransient<UseCasesFile, UseCasesFile>();


var app = builder.Build();

app.UseCorsForClients("http://localhost:3000");

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
