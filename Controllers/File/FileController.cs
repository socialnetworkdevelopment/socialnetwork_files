using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Net;
using UseCases;
using UseCases.File;

namespace Controllers.File;

[ApiController]
[Route("api")]
public class FileController : ControllerBase
{
    private readonly ILogger<FileController> _logger;
    private readonly UseCasesFile _useCasesFile;

    public FileController(ILogger<FileController> logger, UseCasesFile useCasesFile)
    {
        _logger = logger;
        _useCasesFile = useCasesFile;
    }

    [HttpPost]
    [RequestSizeLimit(50000000)]
    public async Task<IActionResult> CreateFile([FromForm]IFormFile file)
    {
        try
        {
            if (file.Length > 0)
            {
                using (var ms = new MemoryStream())
                {
                    file.CopyTo(ms);
                    var fileBytes = ms.ToArray();
                    var base64Str = Convert.ToBase64String(fileBytes);
                    var id = await _useCasesFile.AddFile(new FileDto()
                    {
                        Name = file.FileName,
                        ContentType = file.ContentType,
                        Data = base64Str,
                        DateCreated = DateTime.UtcNow
                    });
                    return StatusCode(201, id);
                }
            }
            else
                return StatusCode(400, "File size must not be zero.");
        }
        catch (Exception ex)
        {
            return StatusCode(500,ex.Message);
        }
    }

    [HttpGet]
    public async Task<IActionResult> GetFile(long id)
    {
        var res = await _useCasesFile.GetFileById(id);
        if (res == null)
            return StatusCode(404);
        return StatusCode(200,res);
    }
    [HttpDelete]
    public async Task<IActionResult> DeleteFile(long id)
    {
        _useCasesFile.DeleteFileById(id);
        return StatusCode(204);
    }
}
