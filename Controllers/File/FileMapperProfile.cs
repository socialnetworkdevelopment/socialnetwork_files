﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UseCases.File;
using Utils;

namespace Controllers.File
{
    public class FileMapperProfile:Profile
    {
        public FileMapperProfile()
        {
            CreateMap<FileDto, Entities.File>().ReverseMap();
        }
    }


    namespace Registration
    {
        public class ChatMapperProfileRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                services.AddAutoMapper((mapperConfiguration) =>
                {
                    mapperConfiguration.AddProfile(new FileMapperProfile());
                });
            }
        }
    }

}
