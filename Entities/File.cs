﻿namespace Entities
{
    public class File : BaseEntity<long>
    {
        public string Name { get; set; }
        public string ContentType { get; set; }
        public string Data { get; set; }
        public DateTime DateCreated { get; set; }
    }
}