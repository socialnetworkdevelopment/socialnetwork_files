﻿using AutoMapper;
using DataAccess.Iterfaces;
using System.Text;
using UseCases.File;

namespace UseCases
{
    public class UseCasesFile
    {
        private readonly IFileRepository _fileRepository;
        private readonly IMapper _mapper;
        public UseCasesFile(IFileRepository rep,IMapper mapper)
        {
            _fileRepository = rep;
            _mapper = mapper;
        }

        public async Task<long> AddFile(FileDto newFileDto)
        {
            var newFile = _mapper.Map<FileDto, Entities.File>(newFileDto);
            var result = await _fileRepository.AddAsync(newFile);
            _fileRepository.SaveChanges();
            return result.Id;
        }
        public async Task<FileDto> GetFileById(long id)
        {
            var file = await _fileRepository.GetById(id);
            return _mapper.Map<Entities.File,FileDto>(file);
        }

        public async void DeleteFileById(long id)
        {
            _fileRepository.DeleteById(id);
            _fileRepository.SaveChanges();
        }
    }
} 